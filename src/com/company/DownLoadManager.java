package com.company;

import java.io.*;
import java.net.URL;

public class DownLoadManager implements Runnable {

    private String imageUrl;

    public DownLoadManager(String imageUrl){
        this.imageUrl= imageUrl;
    }

    public static void downloadImage(String imageUrl) throws IOException {

        InputStream inputStream = null;
        OutputStream outputStream = null;

        try {
            URL url = new URL(imageUrl);
            String fileName = url.getPath();
            String destination = fileName.substring(fileName.lastIndexOf("/") + 1);

            inputStream = url.openStream();
            outputStream = new FileOutputStream(destination);

            byte[] buffer = new byte[2048];
            int length;

            while ((length = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, length);
            }

        }catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException :- " + e.getMessage());

        } catch (IOException e) {
            System.out.println("IOException :- " + e.getMessage());

        } finally {
            try {

                inputStream.close();
                outputStream.close();

            } catch (IOException e) {
                System.out.println("Finally IOException :- " + e.getMessage());
            }
        }
    }

    @Override
    public void run() {
        try {
            downloadImage(imageUrl);
            Thread.sleep(1000);
        } catch (IOException | InterruptedException e) {
            System.out.println("IOException :- " + e.getMessage());
        }
    }
}
