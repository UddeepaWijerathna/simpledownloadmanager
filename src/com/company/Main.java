package com.company;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;



public class Main {

    public static void main(String[] args) {

        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);

        List<String> list = Arrays.asList("https://homepages.cae.wisc.edu/~ece533/images/airplane.png" ,
                "https://homepages.cae.wisc.edu/~ece533/images/arctichare.png" ,
                "https://homepages.cae.wisc.edu/~ece533/images/baboon.png",
                "https://homepages.cae.wisc.edu/~ece533/images/boat.png" ,
                "https://homepages.cae.wisc.edu/~ece533/images/cat.png" ,
                "https://homepages.cae.wisc.edu/~ece533/images/fruits.png" ,
                "https://homepages.cae.wisc.edu/~ece533/images/frymire.png" ,
                "https://homepages.cae.wisc.edu/~ece533/images/girl.png" ,
                "https://homepages.cae.wisc.edu/~ece533/images/monarch.png" ,
                "https://homepages.cae.wisc.edu/~ece533/images/peppers.png");


        for(String element : list) {
            DownLoadManager downLoadManager = new DownLoadManager(element);
            System.out.println("Created : " + downLoadManager.hashCode());

            executor.execute(downLoadManager);
        }
        executor.shutdown();

    }
}
